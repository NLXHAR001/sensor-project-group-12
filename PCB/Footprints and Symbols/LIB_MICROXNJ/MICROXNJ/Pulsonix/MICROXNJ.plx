PULSONIX_LIBRARY_ASCII "SamacSys ECAD Model"
//12053614/776903/2.49/11/4/Connector

(asciiHeader
	(fileUnits MM)
)
(library Library_1
	(padStyleDef "r125_40"
		(holeDiam 0)
		(padShape (layerNumRef 1) (padShapeType Rect)  (shapeWidth 0.400) (shapeHeight 1.250))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 0) (shapeHeight 0))
	)
	(padStyleDef "c130_h70"
		(holeDiam 0.7)
		(padShape (layerNumRef 1) (padShapeType Ellipse)  (shapeWidth 1.300) (shapeHeight 1.300))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 1.300) (shapeHeight 1.300))
	)
	(padStyleDef "c190_h130"
		(holeDiam 1.3)
		(padShape (layerNumRef 1) (padShapeType Ellipse)  (shapeWidth 1.900) (shapeHeight 1.900))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 1.900) (shapeHeight 1.900))
	)
	(padStyleDef "r135_60"
		(holeDiam 0)
		(padShape (layerNumRef 1) (padShapeType Rect)  (shapeWidth 0.600) (shapeHeight 1.350))
		(padShape (layerNumRef 16) (padShapeType Ellipse)  (shapeWidth 0) (shapeHeight 0))
	)
	(textStyleDef "Normal"
		(font
			(fontType Stroke)
			(fontFace "Helvetica")
			(fontHeight 1.27)
			(strokeWidth 0.127)
		)
	)
	(patternDef "MICROXNJ" (originalName "MICROXNJ")
		(multiLayer
			(pad (padNum 1) (padStyleRef r125_40) (pt 1.300, -4.775) (rotation 0))
			(pad (padNum 2) (padStyleRef r125_40) (pt 0.650, -4.775) (rotation 0))
			(pad (padNum 3) (padStyleRef r125_40) (pt 0.000, -4.775) (rotation 0))
			(pad (padNum 4) (padStyleRef r125_40) (pt -0.650, -4.775) (rotation 0))
			(pad (padNum 5) (padStyleRef r125_40) (pt -1.300, -4.775) (rotation 0))
			(pad (padNum 6) (padStyleRef c130_h70) (pt 2.425, -4.800) (rotation 90))
			(pad (padNum 7) (padStyleRef c130_h70) (pt -2.425, -4.800) (rotation 90))
			(pad (padNum 8) (padStyleRef c190_h130) (pt 3.600, -2.130) (rotation 90))
			(pad (padNum 9) (padStyleRef c190_h130) (pt -3.600, -2.130) (rotation 90))
			(pad (padNum 10) (padStyleRef r135_60) (pt 0.800, -2.125) (rotation 0))
			(pad (padNum 11) (padStyleRef r135_60) (pt -0.800, -2.125) (rotation 0))
		)
		(layerContents (layerNumRef 18)
			(attr "RefDes" "RefDes" (pt 0.000, -3.000) (textStyleRef "Normal") (isVisible True))
		)
		(layerContents (layerNumRef 28)
			(line (pt -3.6 0) (pt 3.6 0) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt 3.6 0) (pt 3.6 -5.06) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt 3.6 -5.06) (pt -3.6 -5.06) (width 0.025))
		)
		(layerContents (layerNumRef 28)
			(line (pt -3.6 -5.06) (pt -3.6 0) (width 0.025))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt -5.05 0.5) (pt 5.05 0.5) (width 0.1))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt 5.05 0.5) (pt 5.05 -6.5) (width 0.1))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt 5.05 -6.5) (pt -5.05 -6.5) (width 0.1))
		)
		(layerContents (layerNumRef Courtyard_Top)
			(line (pt -5.05 -6.5) (pt -5.05 0.5) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt 1.2 -5.9) (pt 1.2 -5.9) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(arc (pt 1.3, -5.9) (radius 0.1) (startAngle 180.0) (sweepAngle 180.0) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt 1.4 -5.9) (pt 1.4 -5.9) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(arc (pt 1.3, -5.9) (radius 0.1) (startAngle .0) (sweepAngle 180.0) (width 0.2))
		)
		(layerContents (layerNumRef 18)
			(line (pt -3.6 -3.5) (pt -3.6 -5.06) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -3.6 -5.06) (pt -3.6 -5.06) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -3.6 -5.06) (pt -3.5 -5.06) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt 3.6 -3.5) (pt 3.6 -5.06) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt 3.6 -5.06) (pt 3.5 -5.06) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -3.6 -0.5) (pt -3.6 0) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt -3.6 0) (pt 3.6 0) (width 0.1))
		)
		(layerContents (layerNumRef 18)
			(line (pt 3.6 0) (pt 3.6 -0.5) (width 0.1))
		)
	)
	(symbolDef "MICROXNJ" (originalName "MICROXNJ")

		(pin (pinNum 1) (pt 0 mils 0 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -25 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 2) (pt 0 mils -100 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -125 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 3) (pt 0 mils -200 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -225 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 4) (pt 0 mils -300 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -325 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 5) (pt 0 mils -400 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -425 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 6) (pt 0 mils -500 mils) (rotation 0) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 230 mils -525 mils) (rotation 0]) (justify "Left") (textStyleRef "Normal"))
		))
		(pin (pinNum 7) (pt 1000 mils 0 mils) (rotation 180) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 770 mils -25 mils) (rotation 0]) (justify "Right") (textStyleRef "Normal"))
		))
		(pin (pinNum 8) (pt 1000 mils -100 mils) (rotation 180) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 770 mils -125 mils) (rotation 0]) (justify "Right") (textStyleRef "Normal"))
		))
		(pin (pinNum 9) (pt 1000 mils -200 mils) (rotation 180) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 770 mils -225 mils) (rotation 0]) (justify "Right") (textStyleRef "Normal"))
		))
		(pin (pinNum 10) (pt 1000 mils -300 mils) (rotation 180) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 770 mils -325 mils) (rotation 0]) (justify "Right") (textStyleRef "Normal"))
		))
		(pin (pinNum 11) (pt 1000 mils -400 mils) (rotation 180) (pinLength 200 mils) (pinDisplay (dispPinName true)) (pinName (text (pt 770 mils -425 mils) (rotation 0]) (justify "Right") (textStyleRef "Normal"))
		))
		(line (pt 200 mils 100 mils) (pt 800 mils 100 mils) (width 6 mils))
		(line (pt 800 mils 100 mils) (pt 800 mils -600 mils) (width 6 mils))
		(line (pt 800 mils -600 mils) (pt 200 mils -600 mils) (width 6 mils))
		(line (pt 200 mils -600 mils) (pt 200 mils 100 mils) (width 6 mils))
		(attr "RefDes" "RefDes" (pt 850 mils 300 mils) (justify Left) (isVisible True) (textStyleRef "Normal"))
		(attr "Type" "Type" (pt 850 mils 200 mils) (justify Left) (isVisible True) (textStyleRef "Normal"))

	)
	(compDef "MICROXNJ" (originalName "MICROXNJ") (compHeader (numPins 11) (numParts 1) (refDesPrefix J)
		)
		(compPin "1" (pinName "1") (partNum 1) (symPinNum 1) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "2" (pinName "2") (partNum 1) (symPinNum 2) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "3" (pinName "3") (partNum 1) (symPinNum 3) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "4" (pinName "4") (partNum 1) (symPinNum 4) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "5" (pinName "5") (partNum 1) (symPinNum 5) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "MH1" (pinName "MH1") (partNum 1) (symPinNum 6) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "MH2" (pinName "MH2") (partNum 1) (symPinNum 7) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "MH3" (pinName "MH3") (partNum 1) (symPinNum 8) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "MH4" (pinName "MH4") (partNum 1) (symPinNum 9) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "MP1" (pinName "MP1") (partNum 1) (symPinNum 10) (gateEq 0) (pinEq 0) (pinType Unknown))
		(compPin "MP2" (pinName "MP2") (partNum 1) (symPinNum 11) (gateEq 0) (pinEq 0) (pinType Unknown))
		(attachedSymbol (partNum 1) (altType Normal) (symbolName "MICROXNJ"))
		(attachedPattern (patternNum 1) (patternName "MICROXNJ")
			(numPads 11)
			(padPinMap
				(padNum 1) (compPinRef "1")
				(padNum 2) (compPinRef "2")
				(padNum 3) (compPinRef "3")
				(padNum 4) (compPinRef "4")
				(padNum 5) (compPinRef "5")
				(padNum 6) (compPinRef "MH1")
				(padNum 7) (compPinRef "MH2")
				(padNum 8) (compPinRef "MH3")
				(padNum 9) (compPinRef "MH4")
				(padNum 10) (compPinRef "MP1")
				(padNum 11) (compPinRef "MP2")
			)
		)
		(attr "Manufacturer_Name" "Shou Han")
		(attr "Manufacturer_Part_Number" "MICROXNJ")
		(attr "Mouser Part Number" "")
		(attr "Mouser Price/Stock" "")
		(attr "Arrow Part Number" "")
		(attr "Arrow Price/Stock" "")
		(attr "Description" "MICRO-USB-SMD_MICROXNJ")
		(attr "<Hyperlink>" "https://datasheet.lcsc.com/szlcsc/1912111437_SHOU-HAN-MicroXNJ_C404969.pdf")
		(attr "<Component Height>" "2.425")
		(attr "<STEP Filename>" "MICROXNJ.stp")
		(attr "<STEP Offsets>" "X=0;Y=0;Z=0")
		(attr "<STEP Rotation>" "X=0;Y=0;Z=0")
	)

)
