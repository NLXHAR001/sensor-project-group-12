PCBNEW-LibModule-V1  2022-04-07 23:07:17
# encoding utf-8
Units mm
$INDEX
DIPS530W56P200L1100H525Q10N
$EndINDEX
$MODULE DIPS530W56P200L1100H525Q10N
Po 0 0 0 15 624f6ea5 00000000 ~~
Li DIPS530W56P200L1100H525Q10N
Cd dip-10
Kw Integrated Circuit
Sc 0
At STD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "IC**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "DIPS530W56P200L1100H525Q10N"
DS -1.225 -1.875 6.525 -1.875 0.05 24
DS 6.525 -1.875 6.525 9.875 0.05 24
DS 6.525 9.875 -1.225 9.875 0.05 24
DS -1.225 9.875 -1.225 -1.875 0.05 24
DS -0.975 -1.625 6.275 -1.625 0.1 24
DS 6.275 -1.625 6.275 9.625 0.1 24
DS 6.275 9.625 -0.975 9.625 0.1 24
DS -0.975 9.625 -0.975 -1.625 0.1 24
DS -0.975 -0.625 0.025 -1.625 0.1 24
DS -0.975 9.625 6.275 9.625 0.2 21
DS 6.275 -1.625 -0.975 -1.625 0.2 21
DS -0.975 -1.625 -0.975 0 0.2 21
$PAD
Po 0 0
Sh "1" R 1.275 1.275 0 0 900
Dr 0.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0 2
Sh "2" C 1.275 1.275 0 0 900
Dr 0.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0 4
Sh "3" C 1.275 1.275 0 0 900
Dr 0.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0 6
Sh "4" C 1.275 1.275 0 0 900
Dr 0.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 0 8
Sh "5" C 1.275 1.275 0 0 900
Dr 0.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 5.3 8
Sh "6" C 1.275 1.275 0 0 900
Dr 0.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 5.3 6
Sh "7" C 1.275 1.275 0 0 900
Dr 0.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 5.3 4
Sh "8" C 1.275 1.275 0 0 900
Dr 0.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 5.3 2
Sh "9" C 1.275 1.275 0 0 900
Dr 0.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 5.3 0
Sh "10" C 1.275 1.275 0 0 900
Dr 0.85 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE DIPS530W56P200L1100H525Q10N
$EndLIBRARY
