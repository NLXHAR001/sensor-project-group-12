# Getting Started with Making Best Buds

### Name
EEE3088F Group 12 Sensor Project/Making Best Buds

### Description of our sensor HAT
The HAT is designed to be used to control an automatic plant watering system. The system features an ambient light sensor, a relative humidity sensor and a temperature sensor. 

## Tutorial

### Prerequisites
#### Hardware
A steady 5V supply, STM32 board and a micro usb to connect the PCB to a computer.
#### Software
STM Cube IDE and a proficiency in coding.
#### Set up
Set up the board as instructed in step 1 and 2 of the tutorial.

### Steps
#### Power
This circuit is powered off one 186650 Li-ion battery, which can be inserted into the battery holder on the PCB. This battery will supply power to all the components after being regulated down to 3.3V. Make sure the battery is charged by plugging the USB into a 5V supply and then into the board and charging for at least an hour.
#### Connect your STM Board
Set your board up so it sits with the board in the correct orientation as shown on the board. Connect the STM to a computer so that it can interface and start up. The board needs to connect via a micro-USB connection.
#### Load the sample code for STM cube:
Load the sample code and transmit it to the board using STM Cube IDE. A proficiency in STM Cube IDE is needed.
#### Read the Data Points:
The data points should be shown on the Display and a check should be done to see they are manipulated correctly. The ambient light sensor measures the magnitude of the luminescence of the ambient light and will output a relative reading, meaning that the output will be on a scale of 1 to ten where 0 is 0 lux and 10 is approximately 2000lux. For the temperature reading the actual temperature will be displayed on the segment display with a typical accuracy of ± 0.3° for the range of temperatures between 0° and 60°. The temperature sensor measures temperature values from -40° to 120°. For single digit temperatures the display will show the temperature up to one decimal point, and for two-digit temperatures the display will show only integer values and for three-digit values, the output will show the two most significant digits followed by a decimal point. With regards to the relative humidity, the actual percentage value will be shown on the display, and for single digit percentages, this will be up to one decimal point.
#### Final
The board should be working as described in the product info.
#### Steps to take if not working
If the board is not functioning, important connections that are available through the test point connections include a 3.3V line power rail, a 3V voltage that comes 4 from the UART bridge’s internal voltage regulator, and connections to all communication lines, including, the clock and data lines on the I2C bus connection, the UART transmit and receive lines. If the board is still not functioning read the FAQs, the Explainer or contact the support team at NLXHAR001@myuct.ac.za.

### Contributors
Sigrid Aadnesgaard, Adam Speed-Andrews and Harry Nel.

### Liscense
This project is licensed under the Creative Commons Attribution 4.0 International license. The full license is found in the files.
